
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("Preenche todos os campos de um texto", () => {
        const firstName = "Riciery"
        const lastName = "Spardelotti"

        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastName)
        cy.get('#email').type("riciery@teste.com")
        cy.get("#requests").type("Qualquer coisa aqui")
        cy.get("#signature").type(`${firstName} ${lastName}`)
    });

    it("Selecionar dois tickets", () => {
        cy.get("#ticket-quantity").select("2")
    });
    

    it("Mudar o tipo do ticket para VIP", () => {
        cy.get("#vip").check()
    });

    it("Selecionar Social Midia sobre o evento", () => {
        cy.get("#social-media").check()
    });

    it("Selecionar Friend e Publication, e então desmarcar Friend", () => {
        cy.get("#friend").check()
        cy.get("#publication").check()
        cy.get("#friend").uncheck()
    });
 
    it("tem o texto 'TICKETBOX' no header", () => {
        cy.get('header h1').should('contain', 'TICKETBOX')
    });

    it('alerta em um email inválido', () => {
        cy.get('#email')
          .as('email')
          .type('talkabouttesting-gmail.com')

        cy.get('#email.invalid').should('exist')

        cy.get('@email')
          .clear()
          .type('talkabouttesting@gmail.com')

        cy.get('#email.invalid').should('not.exist')
    });

    it('informar todos os campos, e então resetar o formulário', () => {
        const firstName = "Riciery"
        const lastName = "Spardelotti"
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastName)
        cy.get('#email').type("riciery@teste.com")
        cy.get("#ticket-quantity").select("2")
        cy.get("#vip").check()
        cy.get("#friend").check()
        cy.get("#requests").type("Craft beer")

        cy.get('.agreement p').should(
          'contain', 
          `I, ${fullName}, wish to buy 2 VIP tickets.`)

        cy.get('#agree').click()
        cy.get('#signature').type(fullName)

        cy.get('button[type="submit"]')
          .as('submitButton')
          .should('not.be.disabled')

        cy.get('button[type="reset"]').click()

        cy.get('@submitButton').should('be.disabled')
    });

    it('preencher apenas campos obrigatórios usando o commands', () => {
        const  customer = {
            firstName: 'João',
            lastName: 'Silva',
            email: 'joaosilva@example.com'
        };

        cy.fillMandatoryFields(customer)

        cy.get('button[type="submit"]')
          .as('submitButton')
          .should('not.be.disabled')

        cy.get('#agree').uncheck()

        cy.get('@submitButton').should('be.disabled')
    });
});